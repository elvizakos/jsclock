(function ( ) {

	var htmlClockClass = 'html-clock';
	var svgClockClass = 'svg-clock';
	var canvasClockClass = 'canvas-clock';

	var clockInstances = [];

	var htmlClock = function ( el ) {

		clockInstances.push ( this );

		this.type = 'htmlClock';

		this.props = {};

		this.props.datenow = new Date();
		this.props.date = new Date();
		this.props.height = null;
		this.props.width = null;
		this.props.bodyimg = null;
		this.props.hoursimg = null;
		this.props.minutessimg = null;
		this.props.secondsimg = null;

		this.props.smoothHourPointer = true;
		this.props.smoothMinutesPointer = false;
		this.props.smoothSecondsPointer = false;

		this.props.state = 'start';

		this.elements = {};

		this.elements.container = document.createElement('div');
		this.elements.container.style.overflow = 'hidden';
		this.elements.container.style.backgroundSize = 'cover';
		this.elements.container.dataClockObject = this;

		this.elements.hours = document.createElement('div');
		this.elements.hours.dataClockObject = this;
		this.elements.container.appendChild(this.elements.hours);

		this.elements.minutes = document.createElement('div');
		this.elements.minutes.style.position = 'relative';
		this.elements.minutes.dataClockObject = this;
		this.elements.container.appendChild(this.elements.minutes);

		this.elements.seconds = document.createElement('div');
		this.elements.seconds.style.position = 'relative';
		this.elements.seconds.dataClockObject = this;
		this.elements.container.appendChild(this.elements.seconds);

		this.stop = function () {
			this.props.state = 'stopped';
			return this;
		}

		this.start = function () {
			this.props.state = 'start';
			return this;
		}

		this.setTime = function ( d ) {

			this.props.datenow = new Date();

			if ( d instanceof Date ) this.props.date = d;
			else if ( typeof d == 'string' || typeof d == 'number' ) this.props.date = new Date ( d ) ;
			else if ( typeof d == 'undefined' ) this.props.date = new Date();

			this.draw();

			return this;
		}

		this.resize = function () {
			if ( el ) {
				el.style.height = this.props.height + 'px';
				el.style.width = this.props.width + 'px';
			}
			this.elements.container.style.height = this.props.height + 'px'
			this.elements.hours.style.height = this.props.height + 'px';
			this.elements.minutes.style.height = this.props.height + 'px';
			this.elements.seconds.style.height = this.props.height + 'px';

			this.elements.container.style.width = this.props.width + 'px'
			this.elements.hours.style.width = this.props.width + 'px';
			this.elements.minutes.style.width = this.props.width + 'px';
			this.elements.seconds.style.width = this.props.width + 'px';

			this.elements.minutes.style.top = - this.props.height + 'px';
			this.elements.seconds.style.top = - (this.props.height*2) + 'px';

			this.elements.container.style.backgroundSize = this.props.width + "px " + this.props.height + "px";
			this.elements.hours.style.backgroundSize = this.props.width + "px " + this.props.height + "px";
			this.elements.minutes.style.backgroundSize = this.props.width + "px " + this.props.height + "px";
			this.elements.seconds.style.backgroundSize = this.props.width + "px " + this.props.height + "px";
			return this;

		}

		this.setHeight = function ( h ) {
			if ( h !== null ) {
				this.props.height = h;
				this.elements.container.style.height = this.props.height + 'px';
				this.elements.hours.style.height = this.props.height + 'px';
				this.elements.minutes.style.height = this.props.height + 'px';
				this.elements.minutes.style.top = - this.props.height + 'px';
				this.elements.seconds.style.height = this.props.height + 'px'
				this.elements.seconds.style.top = - (this.props.height*2) + 'px';
			}
			return this;
		};

		this.setWidth = function ( w ) {
			if ( w !== null ) {
				this.props.width = w;
				this.elements.container.style.width = this.props.width + 'px';
				this.elements.hours.style.width = this.props.width + 'px';
				this.elements.minutes.style.width = this.props.width + 'px';
				this.elements.seconds.style.width = this.props.width + 'px';
			}
			return this;
		};

		this.setBodyImage = function ( src ) {
			if ( src !== null ) this.props.bodyimg = src;
			return this;
		};

		this.setHoursImage = function ( src ) {
			if ( src !== null ) this.props.hoursimg = src;
			return this;
		};

		this.setMinutesImage = function ( src ) {
			if ( src !== null ) this.props.minutessimg = src;
			return this;
		};

		this.setSecondsImage = function ( src ) {
			if ( src !== null ) this.props.secondsimg = src;
			return this;
		};

		this.draw = function () {
			var tmpdt = new Date ( this.props.date.getTime() + ( Date.now() - this.props.datenow.getTime() ) );
			var hrs = tmpdt.getHours(); if ( hrs > 12 ) hrs -= 12;
			var min = tmpdt.getMinutes();
			var scs = tmpdt.getSeconds();

			if ( this.props.bodyimg     ) this.elements.container.style.backgroundImage = 'url("'+this.props.bodyimg+'")';
			if ( this.props.hoursimg    ) this.elements.hours.style.backgroundImage = 'url("'+this.props.hoursimg+'")';
			if ( this.props.minutessimg ) this.elements.minutes.style.backgroundImage = 'url("'+this.props.minutessimg+'")';
			if ( this.props.secondsimg  ) this.elements.seconds.style.backgroundImage = 'url("'+this.props.secondsimg+'")';
			
			hrs /= 12;
			if ( this.props.smoothHourPointer == true ) hrs += min / 720;

			min /= 60;
			if (this.props.smoothMinutesPointer == true ) min += scs / 3600;

			scs /= 60;
			if ( this.props.smoothSecondsPointer == true ) {
				var ms = tmpdt.getMilliseconds();
				scs += ms / 60000;
			}

			this.elements.hours.style.transform = "rotate("+ ( hrs * 360) +"deg)";
			this.elements.minutes.style.transform = "rotate("+ ( min * 360) +"deg)";
			this.elements.seconds.style.transform = "rotate("+ ( scs * 360) +"deg)";

			return this;
		}

		if ( typeof el != 'undefined' ) {
			el.dataClockObject = this;
			el.appendChild( this.elements.container );

			var height = el.getAttribute('data-height');
			var width = el.getAttribute('data-width');
			var bodyimg = el.getAttribute('data-bodyimg-src');
			var hoursimg = el.getAttribute('data-hours-src');
			var minutessimg = el.getAttribute('data-minutes-src');
			var secondsimg = el.getAttribute('data-seconds-src');

			if ( el.getAttribute('data-smooth-hours-pointer') == 'true' ) this.props.smoothHourPointer = true;
			else if ( el.getAttribute('data-smooth-hours-pointer') == 'false' ) this.props.smoothHourPointer = false;

			if ( el.getAttribute('data-smooth-minutes-pointer') == 'true' ) this.props.smoothMinutesPointer = true;
			else if ( el.getAttribute('data-smooth-minutes-pointer') == 'false' ) this.props.smoothMinutesPointer = false;

			if ( el.getAttribute('data-smooth-seconds-pointer') == 'true' ) this.props.smoothSecondsPointer = true;
			else if ( el.getAttribute('data-smooth-seconds-pointer') == 'false' ) this.props.smoothSecondsPointer = false;

			var settime = el.getAttribute('data-set-time');
			if ( settime !== null ) {

				var m = settime.match(/^\s*([0-9]+)[:]([0-9]+)[:]([0-9]+)\s*$/);

				if ( m !== null ) {
					var tmpdate = new Date () ;
					tmpdate.setHours( parseInt(m[1]) );
					tmpdate.setMinutes( parseInt(m[2]) );
					tmpdate.setSeconds( parseInt(m[3]) );
					this.setTime(tmpdate);
				} else {

					var m = settime.match(/^\s*([\-+][0-9]+[hms])\s*([\-+][0-9]+[hms])?\s*([\-+][0-9]+[hms])?\s*$/);

					if ( m !== null ) {

						var tmpdate = new Date () ;
						if ( typeof m[1] != 'undefined' ) switch ( m[1].substr(-1)  ) {
							case 'h': tmpdate.setHours( tmpdate.getHours() + parseInt(m[1].substring(0,m[1].length - 1 )) ); break;
							case 'm': tmpdate.setMinutes( tmpdate.getMinutes() + parseInt(m[1].substring(0,m[1].length - 1 )) ); break;
							case 's': tmpdate.setSeconds( tmpdate.getSeconds() + parseInt(m[1].substring(0,m[1].length - 1 )) ); break;
						}

						if ( typeof m[2] != 'undefined' ) switch ( m[2].substr(-1)  ) {
							case 'h': tmpdate.setHours( tmpdate.getHours() + parseInt(m[2].substring(0,m[2].length - 1 )) ); break;
							case 'm': tmpdate.setMinutes( tmpdate.getMinutes() + parseInt(m[2].substring(0,m[2].length - 1 )) ); break;
							case 's': tmpdate.setSeconds( tmpdate.getSeconds() + parseInt(m[2].substring(0,m[2].length - 1 )) ); break;
						}

						if ( typeof m[3] != 'undefined' ) switch ( m[3].substr(-1)  ) {
							case 'h': tmpdate.setHours( tmpdate.getHours() + parseInt(m[3].substring(0,m[3].length - 1 )) ); break;
							case 'm': tmpdate.setMinutes( tmpdate.getMinutes() + parseInt(m[3].substring(0,m[3].length - 1 )) ); break;
							case 's': tmpdate.setSeconds( tmpdate.getSeconds() + parseInt(m[3].substring(0,m[3].length - 1 )) ); break;
						}

						this.setTime(tmpdate);

					}

				}
				
			}

			if ( height !== null ) {
				this.props.height = height;
				this.elements.container.style.height = this.props.height + 'px';
				this.elements.hours.style.height = this.props.height + 'px';
				this.elements.minutes.style.height = this.props.height + 'px';
				this.elements.minutes.style.top = - this.props.height + 'px';
				this.elements.seconds.style.height = this.props.height + 'px'
				this.elements.seconds.style.top = - (this.props.height*2) + 'px';
			}

			if ( width !== null ) {
				this.props.width = width;
				this.elements.container.style.width = this.props.width + 'px';
				this.elements.hours.style.width = this.props.width + 'px';
				this.elements.minutes.style.width = this.props.width + 'px';
				this.elements.seconds.style.width = this.props.width + 'px';
			}

			if ( height !== null && width !== null ) this.resize();

			this
				.setBodyImage ( bodyimg )
				.setHoursImage ( hoursimg )
				.setMinutesImage ( minutessimg )
				.setSecondsImage ( secondsimg )
				.draw()
			;
		}

	}

	htmlClock.onload = function () {
		var cel = document.getElementsByClassName(htmlClockClass);

		for ( var i = 0; i < cel.length; i++ ) new htmlClock ( cel[i] ) ;

		setInterval( function () {
			for ( var i = 0 ; i < clockInstances.length; i++ ) if (clockInstances[i].props.state != 'stopped' ) clockInstances[i].draw();
		}, 100 );
	};

	htmlClock.loadApp = function ( app ) {

		var c = new lvzwebdesktop.Clock();

		if (typeof app.attr != 'undefined')
			for ( var i in app.attr )
				c.props[i] = app.attr[i];

		var self = lvzwebdesktop(app.container);
		var cont = app.container;
		if ( self === false ) {
			cont.appendChild(c.elements.container);
		}
		else {
			self.clock = c;
			self.attachItem(c.elements.container)
				.addEvent('itemattached', function (e){
					e.target.clock = e.item.dataClockObject;
				})
				.addEvent('resize', function (e) {
					var self = e.target;
					self.clock.props.width=self.Width;
					self.clock.props.height=self.Height;
					self.clock.resize();
					self.clock.draw();
				});
			self.setWidth(100);
			self.setHeight(100);
			self.resizeOnContent(false);
		}
		c.resize();
		c.draw();
	};

	if ( document.readyState == 'complete') htmlClock.onload();
	window.addEventListener ( 'load', htmlClock.onload);

	var url = new URL(document.currentScript.src);
	var c = url.searchParams.get("$");
	if ( c !== null ) window[c].Clock = htmlClock;
	else window.Clock = htmlClock;

}) ( );
